package controller;

import lombok.SneakyThrows;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class CourseService {
    public final UserRepository userRepository;
    public final CourseRepository courseRepository;
    private final LessonRepository lessonRepository;

    public CourseService(UserRepository userRepository, CourseRepository courseRepository, LessonRepository lessonRepository) {
        this.userRepository = userRepository;
        this.courseRepository = courseRepository;
        this.lessonRepository = lessonRepository;
    }

    @Transactional
    public void assignUserToCourse(Long userId, Long courseId) {
        User user = userRepository.findById(userId).orElseThrow();
        Course course = courseRepository.findById(courseId).orElseThrow();
        course.getUsers().add(user);
        user.getCourses().add(course);
        courseRepository.save(course);
    }

    @Transactional
    @SneakyThrows
    public void removeUserFromCourse(Long userId, Long courseId) {
        User user = userRepository.findById(userId)
                .orElseThrow(ChangeSetPersister.NotFoundException::new);
        Course course = courseRepository.findById(courseId)
                .orElseThrow(ChangeSetPersister.NotFoundException::new);
        user.getCourses().remove(course);
        course.getUsers().remove(user);
        courseRepository.save(course);
    }
}