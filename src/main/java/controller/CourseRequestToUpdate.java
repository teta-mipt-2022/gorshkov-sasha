package controller;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class CourseRequestToUpdate {
    @NotBlank(message = "Course author has to be filled")
    private String author;
    @NotBlank(message = "Course title has to be filled")
    private String title;

    public CourseRequestToUpdate(String author, String title) {
        this.author = author;
        this.title = title;
    }
}