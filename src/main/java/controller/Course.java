package controller;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REMOVE;

@Entity
@Table(name = "courses")
public class Course {
    @Getter
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotBlank(message = "Course author have to be filled")
    @Column
    @Getter
    @Setter
    private String author;
    @NotBlank(message = "Course title have to be filled")
    @Column
    @Getter
    @Setter
    private String title;

    @OneToMany(mappedBy = "course", orphanRemoval = true, cascade = {PERSIST, REMOVE})
    @Getter
    @Setter
    private List<Lesson> lessons;

    @ManyToMany
    @Getter
    @Setter
    private Set<User> users;

    @Getter
    @Setter
    private LocalDateTime date;

    public Course() {}

    public Course(Long id, String author, String title) {
        this.id = id;
        this.author = author;
        this.title = title;
    }

    public Course(Course course) {
        id = course.getId();
        author = course.getAuthor();
        title = course.getTitle();
    }

    public void removeLesson(Lesson lesson) {
        lessons.remove(lesson);
    }
}
