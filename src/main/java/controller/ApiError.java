package controller;

public class ApiError extends Exception {
    public ApiError(String message) {
        super(message);
    }
}
