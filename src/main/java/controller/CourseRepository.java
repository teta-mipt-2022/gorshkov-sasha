package controller;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {
    List<Course> findByTitleLike(String title);
    Optional<Course> findById(Long userId);
    List<Course> findAll();
    List<Course> findAllByAuthor(String username);
    default List<Course> findAllByUser(User user) {
        return findAllByAuthor(user.getUsername());
    }
    List<Course> findAllByDateBetween(LocalDateTime begin, LocalDateTime end);
}