package controller;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "lessons")
public class Lesson {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private Long id;

    @Column
    @Getter
    @Setter
    private String title;

    @Lob
    @Column
    @Getter
    @Setter
    private String text;

    @ManyToOne(optional = false)
    @Getter
    @Setter
    private Course course;

    @Getter
    @Setter
    private LocalDateTime date;

    public Lesson() {
    }

    public Lesson(String title, String text, Course course) {
        this.title = title;
        this.text = text;
        this.course = course;
    }
}
