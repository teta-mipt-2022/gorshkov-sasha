package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.List;
import java.util.NoSuchElementException;

import static java.util.Objects.requireNonNullElse;

@RestController
@RequestMapping("/course")
public class CourseController {
    private final CourseService courseService;

    @Autowired
    public CourseController(CourseService courseService) {
        this.courseService = courseService;
    }

    @GetMapping("")
    public List<Course> courseTable() {
        return courseService.courseRepository.findAll();
    }

    @GetMapping("/{id}")
    public Course getCourse(@PathVariable("id") Long id) {
        return courseService.courseRepository.findById(id).orElseThrow();
    }

    @PutMapping("/{id}")
    public void updateCourse(@PathVariable Long id,
                             @Valid @RequestBody CourseRequestToUpdate request) {
        Course course = courseService.courseRepository.findById(id).orElseThrow();
        course.setTitle(request.getTitle());
        course.setAuthor(request.getAuthor());
        courseService.courseRepository.save(course);
    }

    @DeleteMapping("/{id}")
    public void deleteCourse(@PathVariable Long id) {
        courseService.courseRepository.deleteById(id);
    }

    @GetMapping("/filter")
    public List<Course> getCoursesByTitlePrefix(@RequestParam(name = "titlePrefix", required = false) String titlePrefix) {
        return courseService.courseRepository.findByTitleLike(requireNonNullElse(titlePrefix, ""));
    }

    @ExceptionHandler
    public ResponseEntity<Object> handleCourseNotFound(CourseNotFoundException e) {
        return ResponseEntity.notFound().build();
    }

    @ExceptionHandler
    public ResponseEntity<Object> handleErrorService(NoSuchElementException e) {
        return ResponseEntity.badRequest().build();
    }

    @PostMapping("/{courseId}/assign")
    @Transactional
    public void assignUser(
            @PathVariable("courseId") Long courseId,
            @RequestParam("userId") Long userId
    ) {
        courseService.assignUserToCourse(userId, courseId);
    }

    @DeleteMapping("/{courseId}/detach")
    @Transactional
    public void detachUser(
            @PathVariable("courseId") Long courseId,
            @RequestParam("userId") Long userId
    ) {
        courseService.removeUserFromCourse(userId, courseId);
    }
}
