package controller;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface LessonRepository extends JpaRepository<Lesson, Long> {
    List<Lesson> findByDate(LocalDateTime date);
    List<Lesson> findAllByDateBetween(LocalDateTime begin, LocalDateTime end);
}
