package com.gitlab.salex139s;

public class Message {
    public String content;
    public EnrichmentType enrichmentType;

    /**
     * Доступные виды обогащения
     */
    public enum EnrichmentType {
        MSISDN
    }
}
