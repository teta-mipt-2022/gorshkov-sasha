package com.gitlab.salex139s;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.Map;
import java.util.concurrent.ConcurrentMap;

/**
 * Обогатитель сообщений
 */
public record EnrichmentService(ConcurrentMap<String, ConcurrentMap<String, String>> map) {
    /**
     * Обогатить сообщение
     *
     * @param message сообщение
     */
    public void enrich(Message message) {
        // Получаю JsonObject из сообщения
        JsonObject jsonObject = JsonParser.parseString(message.content).getAsJsonObject();
        // Выбираю тип обогащения
        if (message.enrichmentType == Message.EnrichmentType.MSISDN) {
            addMSISDN(jsonObject);
        }
        message.content = jsonObject.toString();
    }

    /**
     * Обогащение по MSISDN
     */
    private void addMSISDN(JsonObject jsonObject) {
        // Получаю MSISDN
        String msisdn = String.valueOf(jsonObject.get("msisdn")).replace("\"", "");
        // Проверка на поле msisdn
        if (msisdn.length() != 11) {
            return;
        }

        // Получаю информацию
        Map<String, String> info = this.map.get(msisdn);

        // Конвертирую Map в JsonElement
        JsonElement jsonMap = JsonParser.parseString((new Gson()).toJson(info));
        // Добавляю Map в JsonObject
        jsonObject.add("enrichment", jsonMap);
    }
}
