package com.gitlab.salex139s;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.*;


@SpringBootTest
@ActiveProfiles("test")
public class UnitTest {

    /**
     * Unit-тестирование
     **/
    @Test(timeout = 3000)
    public void test() throws ExecutionException, InterruptedException, TimeoutException {
        // Map с информацией о пользователях
        ConcurrentMap<String, ConcurrentMap<String, String>> map = getFilledUserMap();
        // Сообщения для обогащения
        ArrayList<Message> messages = getMessages();
        // Создаю Executor
        ExecutorService exec = Executors.newFixedThreadPool(3);
        // Массив Future-ов
        ArrayList<Future<Message>> futures = new ArrayList<>();
        // Запускаю потоки
        for (int i = 0; i < 3; i++) {
            int finalI = i;
            futures.add(exec.submit(() -> {
                try {
                    return enrichment(map, messages.get(finalI));
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }));
        }
        // Ожидаю результат и проверяю на корректность
        String first = "{\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"88000001100\",\"enrichment\":{\"firstName\":\"Aleksandr\",\"secondName\":\"Aboldin\"}}";
        String second = "{\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"88000002200\",\"enrichment\":{\"firstName\":\"Boris\",\"secondName\":\"Bobrov\"}}";
        String third = "{\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"88000003300\",\"enrichment\":{\"firstName\":\"Culeyman\",\"secondName\":\"Curful\"}}";
        org.junit.Assert.assertEquals(futures.get(0).get(1, TimeUnit.SECONDS).content, first);
        org.junit.Assert.assertEquals(futures.get(1).get(1, TimeUnit.SECONDS).content, second);
        org.junit.Assert.assertEquals(futures.get(2).get(1, TimeUnit.SECONDS).content, third);
        exec.shutdown();
    }

    /**
     * Тестовые сообщения для обогащения
     */
    public static ArrayList<Message> getMessages() {
        ArrayList<Message> arr = new ArrayList<>();

        Message message = new Message();
        message.enrichmentType = Message.EnrichmentType.MSISDN;
        message.content = """
                {
                  "action": "button_click",
                  "page": "book_card",
                  "msisdn": "88000001100"
                }""";
        arr.add(message);

        message = new Message();
        message.enrichmentType = Message.EnrichmentType.MSISDN;
        message.content = """
                {
                  "action": "button_click",
                  "page": "book_card",
                  "msisdn": "88000002200"
                }""";
        arr.add(message);

        message = new Message();
        message.enrichmentType = Message.EnrichmentType.MSISDN;
        message.content = """
                {
                  "action": "button_click",
                  "page": "book_card",
                  "msisdn": "88000003300"
                }""";
        arr.add(message);

        return arr;
    }

    /**
     * Получить Map, заполненный тестовыми данными
     */
    public static ConcurrentMap<String, ConcurrentMap<String, String>> getFilledUserMap() {
        ConcurrentMap<String, ConcurrentMap<String, String>> map = new ConcurrentHashMap<>();
        ConcurrentMap<String, String> tmp = new ConcurrentHashMap<>();
        tmp.put("firstName", "Aleksandr");
        tmp.put("secondName", "Aboldin");
        map.put("88000001100", tmp);

        tmp = new ConcurrentHashMap<>();
        tmp.put("firstName", "Boris");
        tmp.put("secondName", "Bobrov");
        map.put("88000002200", tmp);

        tmp = new ConcurrentHashMap<>();
        tmp.put("firstName", "Culeyman");
        tmp.put("secondName", "Curful");
        map.put("88000003300", tmp);

        return map;
    }

}
